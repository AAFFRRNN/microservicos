package com.amazon.store.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amazon.store.order.domain.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{
	
}
