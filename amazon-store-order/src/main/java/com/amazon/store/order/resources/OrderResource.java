package com.amazon.store.order.resources;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.amazon.store.order.domain.Order;
import com.amazon.store.order.domain.form.PedidoForm;
import com.amazon.store.order.services.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="orders")
@Api(value="Orders", description="Orders API")
public class OrderResource {
	
	@Autowired
	private OrderService service;
	
	@GetMapping("/")
	@ApiOperation(value="Search Order Lists",  consumes=APPLICATION_JSON_VALUE, response=Order.class, responseContainer="List", produces=APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Order>> list(){
		final List<Order> orders=service.list();
		if(orders.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(orders);
	}

	@GetMapping("/{id}")
	@ApiOperation(value="Search for an order by id",  consumes=APPLICATION_JSON_VALUE, produces=APPLICATION_JSON_VALUE, response=Order.class)
	public ResponseEntity<Order> add(@PathVariable("id") Long id){
		final Optional<Order> order=Optional.ofNullable(service.get(id));
		return order.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value="Order Creation",  consumes=APPLICATION_JSON_VALUE, produces=APPLICATION_JSON_VALUE, response=Order.class)
	public ResponseEntity<Void> add(@RequestBody OrderForm form, final HttpServletRequest request) {
		final Order order=service.add(form);
		UriComponentsBuilder location=UriComponentsBuilder
				.fromPath(request.getRequestURL().append("/{id}").toString());
		URI uri=location.buildAndExpand(order.getId()).toUri();
		return ResponseEntity.created(uri).build();
		
	}
	

}
