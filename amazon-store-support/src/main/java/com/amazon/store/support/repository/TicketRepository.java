package com.amazon.store.support.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amazon.store.support.domain.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long>{
	
	List<Ticket> findByEmail(String email);

}
