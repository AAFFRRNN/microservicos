package com.amazon.store.wishlist.events.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

import com.amazon.store.wishlist.events.CustomChannels;
import com.amazon.store.wishlist.events.models.ProdutoChangeModel;
import com.amazon.store.wishlist.service.WishListService;

@EnableBinding(CustomChannels.class)
public class ProdutoChangeHandler {
	
	@Autowired
	private WishListService listaDesejosService;

	private static final Logger logger=LoggerFactory.getLogger(ProdutoChangeHandler.class);

	@StreamListener("inboundProdutoChanges")
	public void loggerSink(ProdutoChangeModel produtoChange) {
		logger.debug("Received a message of type " + produtoChange.getType());
		switch (produtoChange.getAction()) {
		case "UPDATE":
			logger.debug("Received a UPDATE event from the product service for product id {}",
					produtoChange.getProdutoId());
			listaDesejosService.changeProductNameOurOrders(produtoChange.getProdutoId(), produtoChange.getNome());
			break;
		case "DELETE":
			logger.debug("Received a DELETE event from the product service for product id {}",
					produtoChange.getProdutoId());
			listaDesejosService.removeDeleteProduct(produtoChange.getProdutoId());
			break;
		default:
			logger.error("Received an UNKNOWN event from the product service of type {}", produtoChange.getType());
			break;

		}
	}

}
