package com.amazon.store.order.domain.form;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

@Data
public class ItemOrderForm implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long idProduct;
	private String name;
	private String category;
	private String manufacturer;
	private int quantity;
	private BigDecimal price;
	
}
	