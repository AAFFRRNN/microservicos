package com.amazon.store.wishlist.resources.form;

import java.util.List;

import lombok.Data;

@Data
public class WishListForm {
	
	private String email;
	private String name;
	private List<ProductWishListForm> idProduct;
		
}
