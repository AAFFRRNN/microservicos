package com.amazon.store.order.domain;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor(access=AccessLevel.PROTECTED, force = true)
@Builder(toBuilder=true)
@Embeddable
public class ItemOrder {

	private Long id;
	private Long productId;
	private String name;
	private String category;
	private String manufacturer;
	private int quantity;
	private BigDecimal price;
	
}
