package com.amazon.store.support.resources.form;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TicketForm {
	
	private final String description;
	private final String email;
}
