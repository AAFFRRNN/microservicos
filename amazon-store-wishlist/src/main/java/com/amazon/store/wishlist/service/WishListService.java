package com.amazon.store.wishlist.service;

import static java.util.stream.Collectors.toSet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.amazon.store.wishlist.domain.WishList;
import com.amazon.store.wishlist.domain.ProductWishList;
import com.amazon.store.wishlist.repository.WishListRepository;
import com.amazon.store.wishlist.resources.form.ProductWishListForm;

@Service
public class WisListService {

	@Autowired
	private WishListRepository repository;
	
	@Transactional(rollbackFor=Exception.class)
	public WishList newListaDesejos(String name, String email, List<ProductWishListForm> products) {
		final WishList wishList=WishList.builder()
				.email(email)
				.nome(name)
				.products(products.stream().map(product ->
					ProductWishList.builder()
						.category(product.getCategory())
						.manufacturer(product.getManufacturer())
						.id(product.getId())
						.name(product.getName())
					.build()
				).collect(toSet())).build();
		
		return repository.save(wishList);
	}
	
	public WishList get(Long id) {
		return repository.findById(id).get();
	}
	
	@Transactional(rollbackFor=Exception.class)
	public WishList addProduct(Long idWishList, ProductWishList product) {
		final WishList wishLists=get(idWishList);
		wishLists.addProduct(product);
		repository.save(wishLists);
		return wishLists;
	}
	
	@Transactional(rollbackFor=Exception.class)
	public void changeProductNameOurOrders(Long productId, String nome) {
		List<WishList> wishLists=repository.findAll();
		wishLists.stream()
				.flatMap(p -> p.getProducts().stream())
				.filter(product -> product.getId().equals(productId))
				.forEach(product -> product.setName(name));

		repository.saveAll(wishLists);
	}
	
	@Transactional(rollbackFor=Exception.class)
	public void removeDeleteProduct(Long productId) {
		List<WishList> wishLists=repository.findAll();
		wishLists.forEach(product -> product.removeProduct(productId));
		repository.saveAll(wishLists);
	}


	public List<WishList> findByNameOrEmail(String name, String email) {
		return repository.findAllByNomeAndEmail(nome, email);
	}

	public List<WishList> listByEmail(String email) {
		return repository.findAllByEmail(email);
		
	}

	public WishList findById(Long id) {
		return repository.findById(id).get();
	}
}
