package com.amazon.store.wishlist.resources;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.amazon.store.wishlist.domain.WishList;
import com.amazon.store.wishlist.domain.ProductWishList;
import com.amazon.store.wishlist.resources.form.WishListForm;
import com.amazon.store.wishlist.service.WishListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="wishList")
@Api(tags="Wish List", description="Wish List Management API")
public class WishListResource {
	
	@Autowired
	private WishListService service;
	
	@GetMapping("/email/{email}")
	@ApiOperation(value="Search Email Wish Lists",  consumes=APPLICATION_JSON_VALUE, response=WishList.class, responseContainer="List", produces=APPLICATION_JSON_VALUE)
	public ResponseEntity<List<WishList>> listByEmail(@PathVariable String email) {
		final List<WishList> wishLists=service.listByEmail(email);
		if(wishLists.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(wishLists);
	}
	
	@PostMapping("/")
	@ApiOperation(value="Wish List Creation",  consumes=APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> newList(@RequestBody WishListForm form, final HttpServletRequest request) {
		final WishList wishLists=service.newWishList(form.getName(), form.getEmail(), form.getIdProduct());
		UriComponentsBuilder location=UriComponentsBuilder
				.fromPath(request.getRequestURL().append("/{id}").toString());
		URI uri=location.buildAndExpand(wishLists.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<WishList> getByEmailAndName(Long id){
		final Optional<WishList> wishList=Optional.ofNullable(service.findById(id));
		return wishList.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	@PutMapping(path="/{idWishList}/product")
	public ResponseEntity<Void> addProduct(@PathVariable Long idWishList, @RequestBody ProductWishList product) {
		service.addProduct(idWishList, product);
		return ResponseEntity.ok().build();
	}

}
