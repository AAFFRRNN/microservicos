package com.amazon.store.support.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazon.store.support.domain.Ticket;
import com.amazon.store.support.repository.TicketRepository;

@Service
public class TicketService {
	
	@Autowired
	private TicketRepository repository;

	public Optional<Ticket> findById(long id) {
		return repository.findById(id);
	}

	@Transactional
	public Ticket create(String description, String email) {
		final Ticket ticket = Ticket.builder().description(description).email(email).build();
		return repository.save(ticket);
	}

	public List<Ticket> listByEmail(String email) {
		return repository.findByEmail(email);
	}

}
