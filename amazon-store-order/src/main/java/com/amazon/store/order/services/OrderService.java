package com.amazon.store.order.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.amazon.store.order.domain.ItemOrder;
import com.amazon.store.order.domain.Order;
import com.amazon.store.order.domain.form.OrderForm;
import com.amazon.store.order.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private OrderService orderService;
	
	public List<Order> listar() {
		return orderRepository.findAll();
	}

	public Order get(Long id) {
		return orderRepository.findById(id).get();
	}

	@Transactional(rollbackFor = Exception.class)
	public Order add(OrderForm orderForm) {
		
		final BigDecimal valueFreight = orderService.getValueFreight(orderForm.getCep());
		final List<ItemOrder> itens = orderForm.getItens().stream()
			.map(item -> 
					ItemOrder.builder()
						.category(item.getCategory())
						.productId(item.getIdProduct())
						.manufacturer(item.getManufacturer())
						.id(item.getIdProduct())
						.name(item.getName())
					.build()
				).collect(Collectors.toList());
		
		final Order order = Order.builder()
				.client(orderForm.getClient())
				.data(orderForm.getData())
				.email(orderForm.getEmail())
				.valueFreight(valueFreight)
			.itens(itens).build();
		
		return orderRepository.save(order);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void changeProductNameOurOrders(Long productId, String name) {
		List<Order> orders = orderRepository.findAll();
		orders.stream()
				.flatMap(p -> p.getItens().stream())
				.filter(item -> item.getProductId().equals(productId))
				.forEach(item -> item.setName(name));

		orderRepository.saveAll(orders);
		
	}
	

}
