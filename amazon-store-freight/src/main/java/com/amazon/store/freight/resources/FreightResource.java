package com.amazon.store.freight.resources;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amazon.store.freight.service.FreightService;
import io.swagger.annotations.Api;


@RestController
@RequestMapping("freight")
@Api(tags="Freight", description="Calculates Freight of Orders placed")
public class FreightResource {
	
	@Autowired
	private FreightService service;
	
	@GetMapping("/calculate/{cep}")
	public BigDecimal calculate(@PathVariable("cep") String cep) {
		return service.calculate(cep);
	}

}
