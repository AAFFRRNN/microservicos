package com.amazon.store.product.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.amazon.store.product.domain.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {


	List<Product> findAllByCategory(String category);

	List<Product> findByNameIn(String q);
	
	@Query("SELECT p FROM Product p where p.name LIKE CONCAT('%',:q,'%') OR p.category LIKE CONCAT('%',:q,'%')")
	Set<Product> findByKey(@Param("q") String q);
}
