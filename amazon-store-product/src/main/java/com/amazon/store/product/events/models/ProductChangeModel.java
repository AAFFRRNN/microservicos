package com.amazon.store.product.events.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(force=true)
public class ProductChangeModel {
	private final String type;
	private final String action;
	private final Long productId;
	private final String name;
	private final String category;
	private final String manufacturer;
}
