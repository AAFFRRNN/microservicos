package com.amazon.store.product.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import com.amazon.store.product.domain.Product;
import com.amazon.store.product.events.ProductEventSource;
import com.amazon.store.product.repository.ProductRepository;


@Service
public class ProductService {
	
	@Autowired
	private ProductRepository repository;
	@Autowired 
	private ProductEventSource eventService;
	
	@Transactional
	public Product save(final Product product) {
		final Product productSaved = repository.save(product);
		eventService.publishProdutoChange("SAVE", productSaved);
		return productSaved;
	}
	
	@Transactional
	public void remove(final Long id) {
		search(id).ifPresent(product ->{
			eventService.publishProductChange("DELETE", product);
			repository.deleteById(id);
		});;
	} 
	
	@Transactional
	public Product update(final Product product) {
		eventService.publishProductChange("UPDATE", product);
		return repository.save(product);
	} 
	
	public Optional<Product> search(final Long id) {
		return repository.findById(id);
	}
	
	public List<Product> list() {
		return repository.findAll();
	}
	
	public List<Product> listByCategory(String category) {
		return repository.findAllByCategory(category);
	}
	
	public List<Product> listarProdutosPor(final String name, final String category, final String manufacturer) {
		final Product product = Product.builder()
				.categoria(categoria)
				.manufacturer(manufacturer)
				.name(name)
			.build();
		
		return repository.findAll(Example.of(product, ExampleMatcher.matchingAny().withIgnoreCase()));

	}

}