package com.amazon.store.wishlist.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amazon.store.wishlist.domain.WishList;


public interface WishListRepository extends JpaRepository<WishList, Long>{

	List<WishList> findAllByNomeAndEmail(String name, String email) ;

	List<WishList> findAllByEmail(String email);

	List<WishList> findByEmail(String email);

}
