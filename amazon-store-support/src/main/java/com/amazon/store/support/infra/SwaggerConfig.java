package com.amazon.store.support.infra;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)
	          .groupName("amazon-store-support").apiInfo(apiInfo()).select()
	          .apis(RequestHandlerSelectors.basePackage("com.amazon.store.support"))
	          .paths(PathSelectors.any())
          .build();                                           
    }
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Amazon Services Support")
					.description("Problem call services faced by users.")
					.version("1.0.0")
				.build();
	}
	
}
