package com.amazon.store.product.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product_view")
public class ProductView {

	@Id
	private Long idProduct;
	private String category;
	private long quantity;
	
	
	
	public ProductView() {

	}

	public v(Long idProduct, String category) {
		super();
		this.idProduct = idProduct;
		this.category = category;
		this.quantity = 1;
	}
	
	public void view() {
		this.quantity += 1;
	}

	public Long getIdProduct() {
		return idProduto;
	}

	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public String quantityViews() {
		return "View " + quantidade;
	}
	
	
}
