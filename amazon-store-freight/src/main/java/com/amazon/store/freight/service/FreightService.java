package com.amazon.store.freight.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class FreightService {
	
	@HystrixCommand(fallbackMethod="buildFallbackCalculateFreight", threadPoolKey="FreightThreadPool", threadPoolProperties={
			@HystrixProperty(name="coreSize", value="30"),
			@HystrixProperty(name="maxQueueSize", value="10") }, commandProperties={
					@HystrixProperty(name="circuitBreaker.requestVolumeThreshold", value="10"),
					@HystrixProperty(name="circuitBreaker.errorThresholdPercentage", value="75"),
					@HystrixProperty(name="circuitBreaker.sleepWindowInMilliseconds", value="7000"),
					@HystrixProperty(name="metrics.rollingStats.timeInMilliseconds", value="15000"),
					@HystrixProperty(name="metrics.rollingStats.numBuckets", value="5") })
	public BigDecimal calculate(final String cep) {
		try { Thread.sleep(10000l);}catch(Exception ex) {}
		return BigDecimal.valueOf(ThreadLocalRandom.current().nextDouble(10, 50)).setScale(2, RoundingMode.HALF_UP);
	}
	
	public BigDecimal buildFallbackCalculateFreight(final String cep) {
		return BigDecimal.valueOf(60);
	}

}
