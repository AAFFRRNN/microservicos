package com.amazon.store.order.events.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

import com.amazon.store.order.events.CustomChannels;
import com.amazon.store.order.events.models.ProductChangeModel;
import com.amazon.store.order.services.OrderService;

@EnableBinding(CustomChannels.class)
public class ProductChangeHandler {

	private static final Logger logger = LoggerFactory.getLogger(ProductChangeHandler.class);
	
	@Autowired
	private OrderService orderService;

	@StreamListener("inboundProductChanges")
	public void loggerSink(ProductChangeModel productChange) {
		logger.debug("Received a message of type " + productChange.getType());
		switch (productChange.getAction()) {
		
		case "UPDATE":
			logger.debug("Received a UPDATE event from the product service for product id {}",
					productChange.getProductId());
			orderService.changeProductNameOurOrders(productChange.getProductId(), productChange.getName());
			break;
		default:
			logger.error("Received an UNKNOWN event from the product service of type {}", productChange.getType());
			break;
		}
	}

}
