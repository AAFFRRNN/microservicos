package com.amazon.store.product.resources;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.amazon.store.product.domain.Product;
import com.amazon.store.product.domain.ProductView;
import com.amazon.store.product.services.ProductService;
import com.amazon.store.product.services.ProductViewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="product")
@Api(tags="Products", description="Products API")
public class ProdutoResource {
	
	@Autowired
	private ProductService service;
	@Autowired
	private ProductViewService productViewService;  
	
	
	@GetMapping("/")
	@ApiOperation(value="Search Product Lists",  consumes=APPLICATION_JSON_VALUE, response=Product.class, responseContainer="List", produces=APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Product>> list(){
		final List<Product> products=service.list();
		if(products.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(products);
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value="Search for a product by id", response=Product.class, produces=APPLICATION_JSON_VALUE)
	public ResponseEntity<Product> get(@PathVariable Long id){
		final Optional<Product> product=service.find(id);
		product.ifPresent(productViewService::addPreview);
		return product.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
	}

	@GetMapping(path="/search", params= {"name", "category", "manufacturer"})
	@ApiOperation(value="Search Product Lists by name, category or manufacturer",  consumes=APPLICATION_JSON_VALUE, response=Product.class, responseContainer="List", produces=APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Product>> find(
			@RequestParam(name="name", required=false, defaultValue="") String name,
			@RequestParam(name="category", required=false, defaultValue="") String category,
			@RequestParam(name="manufacturer", required=false, defaultValue="") String manufacturer){
		final List<Product> products=service.listProductsBy(name, category, manufacturer);
		if(products.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(products);
	}

	@GetMapping("/mostViewed/{category}")
	@ApiOperation(value="Search product listings more viewer by category",  response=ProductView.class, responseContainer="List", produces=APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProductView>> productsMoreViewsByCategory(@PathVariable("category") String category) {
		final List<ProductView> products=productViewService.productsMoreViewsByCategory(category);;
		if(products.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(products);
	}

	@PostMapping(path="/", consumes=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value="Product Creation Service",  consumes=APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> add(@RequestBody Product form, final HttpServletRequest request) {
		final Product product=service.save(form);
		UriComponentsBuilder location=UriComponentsBuilder
				.fromPath(request.getRequestURL().append("/{id}").toString());
		URI uri=location.buildAndExpand(product.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping(path="/{id}",  consumes=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value="Product Update Service",  consumes=APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> update(@PathVariable("id") Long id, @RequestBody Product product) {
		service.update(product);
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping(path="/{id}")
	@ApiOperation(value="Product Removal Service",  consumes=APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> remove(@PathVariable Long id ) {
		service.remove(id);
		return ResponseEntity.noContent().build();
	}

}