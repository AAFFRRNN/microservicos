INSERT INTO product (id,name, category, manufacturer, price) VALUES (1,'Celular Iphone', 'Celulares','Apple', 5000);
INSERT INTO product (id,name, category, manufacturer, price) VALUES (2,'Celular Moto G7', 'Celulares', 'Motorola', 1500);
INSERT INTO product (id,name, category, manufacturer, price) VALUES (3,'Monitor 23"', 'Monitores', 'LG', 800);
INSERT INTO product (id,name, category, manufacturer, price) VALUES (4,'Monitor 27"', 'Monitores', 'Asus', 1200);
INSERT INTO product (id,name, category, manufacturer, price) VALUES (5,'Monitor 23"', 'Monitores', 'LG', 800);