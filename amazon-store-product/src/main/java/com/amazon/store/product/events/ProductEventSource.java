package com.amazon.store.product.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.amazon.store.product.domain.Product;
import com.amazon.store.product.events.models.ProductChangeModel;

@Component
public class ProductEventSource {

	private static final Logger logger = LoggerFactory.getLogger(ProductEventSource.class);

	@Autowired
	private Source source;

	@Async
	public void publishProductChange(String action, Product product) {
		logger.debug("Sending Kafka message {} for product Id: {}", action, product.getId());
		ProductChangeModel change = new ProductChangeModel(ProductChangeModel.class.getTypeName(),
				action, product.getId(), product.getName(),product.getCategory(), product.getManufacturer());
		source.output().send(MessageBuilder.withPayload(change).build());
	}

}
