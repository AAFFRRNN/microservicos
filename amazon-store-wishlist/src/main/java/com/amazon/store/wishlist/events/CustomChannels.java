package com.amazon.store.wishlist.events;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface CustomChannels {
	
	@Input("inboundProdutoChanges")
	SubscribableChannel orgs();
}