package com.amazon.store.wishlist.resources.form;

import lombok.Data;

@Data
public class ProductWishListForm {
	
	private Long id;
	
	private String name;
	
	private String category;
	
	private String manufacturer;
}
