package com.amazon.store.wishlist.events.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(force=true)
public class ProdutoChangeModel {
	private final String type;
	private final String action;
	private final Long productId;
	private final String nome;
	private final String categoria;
	private final String fabricante;
}
