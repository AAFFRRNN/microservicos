package com.amazon.store.product.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;

import com.amazon.store.product.domain.Product;
import com.amazon.store.product.domain.ProductView;
import com.amazon.store.product.repository.ProductViewRepository;

@Service
public class ProductViewService {

	@Autowired
	private ProductViewRepository repository;
	
	@Transactional(rollbackFor = Exception.class)
	public void addPreview(Product product) {
		if (!repository.findById(product.getId()).isPresent()) {
			repository.save(new ProductView(product.getId(), product.getCategory()));
			return;
		} 
		repository.findById(product.getId()).get().viewed();
	}
	
	public List<ProductView> productsMoreViewsByCategory(String category) {
		return repository.findByCategory(category).stream()
							.sorted(Comparator.comparingLong(ProductView::getQuantity).reversed())
							.collect(Collectors.toList());
	}
}
