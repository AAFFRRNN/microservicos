package com.amazon.store.order.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access=AccessLevel.PROTECTED, force=true)
@Entity
@Table(name="order")
public class Order {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_ORDER")
	private final Long id=null;
	private final String client;
	private final String email;
	
	@Builder.Default
	@Enumerated(EnumType.STRING)
	private final StatusOrder status=StatusOrder.ANALYZING;
	
	@Temporal(TemporalType.TIME)
	private final Date data;
	
	@ElementCollection
	@CollectionTable(name="ITEM_ORDER", joinColumns=@JoinColumn(name="ID_ORDER"))
	private final List<ItemOrder> itens;
	
	@Column(name="FREIGHT_VALUE")
	private BigDecimal valueFreight;

	enum StatusPedido {
		ANALYZING, APPROVED, IN_SEPARATION, SEPARATE, READY_END_ SHIPPED, SENT;
	}

}
