package com.amazon.store.product.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amazon.store.product.domain.ProductView;


public interface ProductViewRepository extends JpaRepository<ProductView, Long>{
	
	List<ProductView> findByCategory(String category);

	
}
