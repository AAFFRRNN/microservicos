package com.amazon.store.wishlist.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.google.common.collect.Sets;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor(force=true, access=AccessLevel.PROTECTED)
@AllArgsConstructor
@Entity
@Table(name="WISHLIST")
public class Wishlist {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private final Long id=null;
	private final String name;
	private final String email;
	
	@Builder.Default
	@ManyToMany(cascade={ CascadeType.ALL })
    @JoinTable(
		        name="product_wish_list",
		        joinColumns={ @JoinColumn(name="wish_list_id") },
		        inverseJoinColumns={ @JoinColumn(name="product_id") }
		    )
	private final Set<ProductWishList> products=Sets.newHashSet();

	public void addProduct(ProductWishList product) {
		this.products.add(product);
	}
	
	public void removeProduct(Long productId) {
		products.removeIf(p -> p.getId().equals(productId));
	}
	
}
